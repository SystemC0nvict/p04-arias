//
//  GameViewController.h
//  Runner Game
//
//  Created by Neo SX on 3/11/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>

@interface GameViewController : UIViewController

@end
