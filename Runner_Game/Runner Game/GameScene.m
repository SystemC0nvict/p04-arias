//
//  GameScene.m
//  Runner Game
//
//  Created by Neo SX on 3/11/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import "GameScene.h"

@interface GameScene() {
    SKSpriteNode* _megaman;
    SKColor* _backC;
}
@end

@implementation GameScene
    //SKShapeNode *_spinnyNode;
    //SKLabelNode *_label;
    //SKSpriteNode* _megaman;
//}

-(id)initWithSize:(CGSize)size{
    if (self = [super initWithSize:size]){
        //color the background with an image
       // _backC = [];
        SKTexture* megatex1 = [SKTexture textureWithImageNamed:@"megav1.jpg"];
        megatex1.filteringMode = SKTextureFilteringNearest;
        _megaman = [SKSpriteNode spriteNodeWithTexture:megatex1];
    //SKTexture* megatex2 = [SKTexture textureWithImageNamed:@"megav1.jpg"];
   // megatex1.filteringMode = SKTextureFilteringNearest;
    //animate jumping
    //SKAction* jump = [SKAction repeatActionForever:[SKAction animateWithTextures:@[megatex1, megatex2] timePerFrame:.2]];
        [_megaman setScale:2.0];
        _megaman.position = CGPointMake(self.frame.size.width / 4, CGRectGetMidY(self.frame));
        [self addChild:_megaman];
        
        
    	}
    return self;
}


//- (void)didMoveToView:(SKView *)view {
    // Setup your scene here
    
       // Get label node from scene and store it for use later
// _label = (SKLabelNode *)[self childNodeWithName:@"//helloLabel"];
    
   // _label.alpha = 0.0;
   // [_label runAction:[SKAction fadeInWithDuration:2.0]];
    
   // CGFloat w = (self.size.width + self.size.height) * 0.05;
    
    // Create shape node to use during mouse interaction
   // _spinnyNode = [SKShapeNode shapeNodeWithRectOfSize:CGSizeMake(w, w) cornerRadius:w * 0.3];
    //_spinnyNode.lineWidth = 2.5;
    
   // [_spinnyNode runAction:[SKAction repeatActionForever:[SKAction rotateByAngle:M_PI duration:1]]];
   // [_spinnyNode runAction:[SKAction sequence:@[
                                                //[SKAction waitForDuration:0.5],
                                               //[SKAction fadeOutWithDuration:0.5],
                                              //  [SKAction removeFromParent],
                                                //]]];
//}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // Run 'Pulse' action from 'Actions.sks'
    
}

-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
}

@end
